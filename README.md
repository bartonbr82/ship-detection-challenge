# Ship-Detection-Challenge

A computer vision project built to detect ships on the open seas

 **NOTE:** this project is written in Python 3 and makes heavy use of the following libraries and these are expected to be installed on your local machine:   
 * pandas
 * numpy
 * keras
 * tensorflow
 * skimage
 * matplotlib
 * plotly
 * sklearn
 

Project Setup: 
 1. Download the data from this URL: https://www.kaggle.com/c/airbus-ship-detection/data
 2. Move downloaded zip file into C:/data
 3. Unzip the image data to C:/data/all/train_v2/
 4. Move the train_ship_segmentations_v2 file to C:/data/all/
 5. Start the Jupyter Notebook found in the root directory of this project
 
 Once the Jupyter notebook is started you should execute code cells from the top down. 
 
 **If you do not wish to train the models:**  
 The most up to date model weights have been included under the /weights directory in this project. Most code cells that call the model manunally will utilize these weights rather than being reliant on re-training the model. 