from tensorflow.keras.utils import Sequence
import numpy as np
from ship_detection.mask_utils.encoding import generate_mask
from keras.preprocessing import image
from ship_detection.augmentation.flipandrotate import flip_and_rotate


class TrainingGenerator(Sequence):

    def __init__(self, x_set, y_set, batch_size, scaling):
        self.x = x_set
        self.y = y_set
        self.batch_size = batch_size
        self.scaling = scaling

    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]

        preprocessed_x = self.paths_to_tensor(batch_x)
        preprocessed_y = [np.expand_dims(generate_mask(mask_encoding, self.scaling), axis=2) for mask_encoding in
                          batch_y]

        for i, [x, y] in enumerate(zip(preprocessed_x, preprocessed_y)):
            preprocessed_x[i], preprocessed_y[i] = flip_and_rotate(x, y)

        return np.array(preprocessed_x), np.array(preprocessed_y)

    def path_to_tensor(self, img_path):
        img = image.load_img(img_path)
        x = image.img_to_array(img)[::self.scaling[0], ::self.scaling[1]]
        return np.expand_dims(x, axis=0)

    def paths_to_tensor(self, img_paths):
        list_of_tensors = [self.path_to_tensor(img_path) for img_path in img_paths]
        return np.vstack(list_of_tensors)
