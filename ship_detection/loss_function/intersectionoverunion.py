import numpy.ma as ma
from tensorflow.keras import backend as K

def IoULoss(truth, prediction):
    intersection = K.sum(truth * prediction, axis=[1, 2, 3])
    union = K.sum(truth,  axis=[1, 2, 3]) + K.sum(prediction, axis=[1, 2, 3]) - intersection
    nonzero = 1e-10
    IOU = K.mean( (intersection + nonzero) / (union + nonzero), axis=0 )
    return 1 - IOU