import numpy as np
from random import randint


def flip_and_rotate(image, mask):
    choice = randint(0, 3)
    if choice == 0:
        return np.fliplr(image), np.fliplr(mask)
    if choice == 1:
        return np.flipud(image), np.flipud(mask)
    if choice == 2:
        return np.rot90(image, 1), np.rot90(mask, 1)
    if choice == 3:
        return np.rot90(image, 2), np.rot90(mask, 2)
    if choice == 4:
        return np.rot90(image, 3), np.rot90(mask, 3)
    if choice == 5:
        return image, mask
