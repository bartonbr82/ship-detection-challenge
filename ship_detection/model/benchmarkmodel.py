from tensorflow.keras.layers import Conv2D, Input
from tensorflow.keras.models import Model
from ship_detection.loss_function.intersectionoverunion import IoULoss

class BenchmarkModel:
    def __init__(self, input_shape):
        self.input_shape = input_shape
        self.model = None
        self.build_model()

    def build_model(self):

        inputs = Input(self.input_shape)
        outputs = Conv2D(1, (1, 1), activation='sigmoid')(inputs)
        self.model = Model(inputs=[inputs], outputs=[outputs])
        self.model.compile(optimizer='adam', loss=IoULoss)
