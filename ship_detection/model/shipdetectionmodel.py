from tensorflow.keras.layers import Conv2D, Conv2DTranspose, Input, MaxPool2D, concatenate, GaussianNoise, BatchNormalization
from tensorflow.keras.models import Model
from ship_detection.loss_function.intersectionoverunion import IoULoss


class ShipDetectionModel:
    def __init__(self, input_shape):
        self.input_shape = input_shape
        self.model = None
        self.build_model()

    def build_model(self):
        inputs = Input(self.input_shape)

        gaussian_noise_input = GaussianNoise(0.1)(inputs)

        b1 = BatchNormalization()(gaussian_noise_input)
        conv1a = Conv2D(8, (3, 3), activation='relu', padding='same')(b1)
        b2 = BatchNormalization()(conv1a)
        conv2a = Conv2D(8, (3, 3), activation='relu', padding='same')(b2)
        pool1a = MaxPool2D((2, 2))(conv2a)

        b3 = BatchNormalization()(pool1a)
        conv1b = Conv2D(16, (3, 3), activation='relu', padding='same')(b3)
        b4 = BatchNormalization()(conv1b)
        conv2b = Conv2D(16, (3, 3), activation='relu', padding='same')(b4)
        pool1b = MaxPool2D((2, 2))(conv2b)

        b5 = BatchNormalization()(pool1b)
        conv1c = Conv2D(32, (3, 3), activation='relu', padding='same')(b5)
        b6 = BatchNormalization()(conv1c)
        conv2c = Conv2D(32, (3, 3), activation='relu', padding='same')(b6)
        pool1c = MaxPool2D((2, 2))(conv2c)

        b7 = BatchNormalization()(pool1c)
        conv1d = Conv2D(64, (3, 3), activation='relu', padding='same')(b7)
        b8 = BatchNormalization()(conv1d)
        conv2d = Conv2D(64, (3, 3), activation='relu', padding='same')(b8)
        pool1d = MaxPool2D((2, 2))(conv2d)

        b9 = BatchNormalization()(pool1d)
        conv1e = Conv2D(128, (3, 3), activation='relu', padding='same')(b9)
        b10 = BatchNormalization()(conv1e)
        conv2e = Conv2D(128, (3, 3), activation='relu', padding='same')(b10)
        trans1e = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv2e)

        concat1f = concatenate([trans1e, conv2d])
        b11 = BatchNormalization()(concat1f)
        conv1f = Conv2D(64, (3, 3), activation='relu', padding='same')(b11)
        b12 = BatchNormalization()(conv1f)
        conv2f = Conv2D(64, (3, 3), activation='relu', padding='same')(b12)
        trans1f = Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(conv2f)

        concat1g = concatenate([trans1f, conv2c])
        b13 = BatchNormalization()(concat1g)
        conv1g = Conv2D(32, (3, 3), activation='relu', padding='same')(b13)
        b14 = BatchNormalization()(conv1g)
        conv2g = Conv2D(32, (3, 3), activation='relu', padding='same')(b14)
        trans1g = Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(conv2g)

        concat1h = concatenate([trans1g, conv2b])
        b15 = BatchNormalization()(concat1h)
        conv1h = Conv2D(16, (3, 3), activation='relu', padding='same')(b15)
        b16 = BatchNormalization()(conv1h)
        conv2h = Conv2D(16, (3, 3), activation='relu', padding='same')(b16)
        trans1h = Conv2DTranspose(8, (2, 2), strides=(2, 2), padding='same')(conv2h)

        concat1i = concatenate([trans1h, conv2a])
        b17 = BatchNormalization()(concat1i)
        conv1i = Conv2D(8, (3, 3), activation='relu', padding='same')(b17)
        b18 = BatchNormalization()(conv1i)
        conv2i = Conv2D(8, (3, 3), activation='relu', padding='same')(b18)

        outputs = Conv2D(1, (1, 1), activation='sigmoid', padding='same')(conv2i)

        self.model = Model(inputs=[inputs], outputs=[outputs])
        self.model.compile(optimizer='adam', loss=IoULoss)
