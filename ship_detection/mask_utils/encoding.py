import numpy as np


def generate_mask(encoded_pixel_list, scaling=None):
    if scaling is None:
        scaling = [1, 1]

    image_mask = np.zeros(768 ** 2)
    encodings = np.array(encoded_pixel_list.split()).astype(int)
    pairs = np.reshape(encodings, (-1, 2))
    for location, stretch in pairs:
        image_mask[location:location+stretch] = 1
    return np.reshape(image_mask, (768, 768)).T[::scaling[0], ::scaling[1]]
